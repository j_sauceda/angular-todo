export default interface Item {
  description: string;
  done: boolean;
}
