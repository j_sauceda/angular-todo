import { Component } from '@angular/core';

import { Filter } from './shared/Filter';
import Item from './shared/item.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Angular Todo';
  appFilter = Filter;

  filters = this.appFilter.all;

  allItems: Item[] = [
    { description: 'Despertar', done: true },
    { description: 'Desayunar', done: true },
  ];

  constructor() {}

  get items() {
    if (this.filters === this.appFilter.all) {
      return this.allItems;
    }
    return this.allItems.filter((item) =>
      this.filters === this.appFilter.done ? item.done : !item.done
    );
  }

  addItem(new_description: string) {
    if (!new_description) return;
    this.allItems.push({
      description: new_description,
      done: false,
    });
  }

  changeStatus(done: boolean, id: number) {
    this.allItems[id].done = done;
  }

  updateDescription(new_description: string, id: number) {
    this.allItems[id].description = new_description;
  }

  removeItem(item: Item, index: number) {
    this.allItems.splice(this.allItems.indexOf(item), 1);
  }
}
